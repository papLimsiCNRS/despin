# Replication experiences of DeSpin 

Installation of required Python librairies:
```bash
pip install -r requirements.txt
```

Must download datasets from [this zenodo page](https://zenodo.org/communities/miror/search?page=1&size=20&q=Koroleva&type=dataset)

## Outcomes token classification

In the folder `outcomes_ner` we replicate the outcome extraction experiences, for primary and reported outcome separately.

1. [`create_hf_dataset.py`](./outcomes_ner/create_hf_dataset.py) file is used to convert the original tsv files to the HuggingFace `datasets` format.
2. [`train.py`](./outcomes_ner/train.py) is the main training file that we use to train different transformers models from HuggingFace. The test set results are also stored in 3 files (for each split) : `metrics.json` (that stores f1-score, precision, recall and accuracy according to seqeval evaluation), `predictions.npy` (that stores the predictions logits in a numpy array format) and `config.json` (that stores the training configuration).
3. [`seqeval_evaluation.py`](./outcomes_ner/seqeval_evaluation.py) uses the previous `metrics.json` files and do the mean of the 10 splits to report global SeqEval metrics.
4. [`error_analysis.py`](./outcomes_ner/error_analysis.py) creates Excel files for each split to see where the models fails.

The results are reported in Markdown and TSV format in the [`global_results`](./outcomes_ner/global_results) folder.

## Outcomes semantic similarity

In the folder `outcomes_similarity`we replicate the outcome semantic similarity experiences. The file are basically the same as before, but for the task of Text Classification ( = Sequence Classification), so the evaluation computation is different but we still report the same metrics.

* we added some experiences using [`sentence_transformers library`](https://www.sbert.net/) in the  as they proved to be efficient for the task of semantic similarity  
    * `train_single_st.py` file : this script trains a single split to be able to parralelize the 10 fold training. The training configurations are stored in `st_configs` directory. Command to run a training on a particular configuration :  `python3 single_train_st.py --config <config_path> `. The evaluation is automatically done and stored in a csv file at the end of training.
    * `st_evaluation.py` uses the evaluation files from the splits to do the mean evaluation.
* in `unsup_st_evaluation.py` we ran an experiment with sentence transformers in an unsupervised setting (only optimising the cosine similarity threshold).
