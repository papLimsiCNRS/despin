|    | model                    |   precision |   recall |       f1 |
|---:|:-------------------------|------------:|---------:|---------:|
|  0 | scibert_scivocab_uncased |    0.541789 | 0.644694 | 0.587114 |
|  1 | biobert-v1.1             |    0.571901 | 0.668314 | 0.614156 |
|  2 | biobert_v1.0_pubmed_pmc  |    0.545956 | 0.642462 | 0.589083 |