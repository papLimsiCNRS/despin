# get the errors in an excel file
# to perform error analysis
from datasets import Dataset, load_from_disk
import numpy as np
import pandas as pd
import os 
from typing import Any, Tuple, List


def get_all_unique_models(outcomes:List[str], current_file_dir:str) -> List[str]:
    models = set()

    dir_to_search = [os.path.join(current_file_dir,outcome,"test_results") for outcome in outcomes]
    for dir in dir_to_search :
        for model in os.listdir(dir):
            models.add(model)
    return models

def load_results(results_path:str,label_list : List[str]) -> Tuple[List[List[str]], List[List[str]]]:
    labels = np.load(os.path.join(results_path,"label_ids.npy"))
    predictions = np.load(os.path.join(results_path,"predictions.npy"))
    predictions = np.argmax(predictions, axis=2)
    true_predictions = [
        [label_list[p] for (p, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]
    true_labels = [
        [label_list[l] for (p, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]
    return true_labels, true_predictions


def get_errors_list(test_set : Dataset, labels: np.ndarray, predictions: np.ndarray) -> List[object]:
    errors = []
    for example in test_set :
        id = example["id"]
        tokens = example["tokens"]
        true_tags = labels[id]
        pred_tags = predictions[id]
        for i in range(len(tokens)):
            if true_tags[i] != pred_tags[i]:
                error = {
                    "sentence_id":id,
                    "token":tokens[i],
                    "true_tag":true_tags[i],
                    "pred_tag":pred_tags[i]}
                errors.append(error)
    return errors

def run_single_error_analysis(outcome:str, model_name:str, data_split:int, label_list:List[str], current_file_dir:str) -> None :
    test_results_path = os.path.join(current_file_dir,outcome,"test_results",model_name,str(data_split))
    dataset_path = os.path.join(current_file_dir,outcome,"data",str(data_split))
    labels, predictions = load_results(test_results_path, label_list)
    test_set = load_from_disk(dataset_path)['test']
    errors = get_errors_list(test_set, labels, predictions)
    error_df = pd.DataFrame(errors)
    error_df.to_excel(os.path.join(current_file_dir,test_results_path,"errors.xlsx"))

def main():
    outcomes = ["primary","reported"]
    current_file_dir = os.path.dirname(os.path.realpath(__file__))
    models = get_all_unique_models(["primary", "reported"], current_file_dir)
    data_splits = range(10)
    for outcome in outcomes :
        label_list = ['O','B-'+outcome.capitalize(),'I-'+outcome.capitalize()]
        for model in models:
            for data_split in data_splits:
                if os.path.isdir(os.path.join(current_file_dir,outcome,"test_results",model,str(data_split))):
                    run_single_error_analysis(outcome, model, data_split, label_list, current_file_dir)

if __name__ == "__main__":
    main()