# coding=utf-8
# author: @laiking
"""Class for named entity recognition on outcomes"""
from datasets import load_from_disk, load_metric, disable_progress_bar
from transformers import (
    AutoModelForTokenClassification,
    AutoTokenizer,
    DataCollatorForTokenClassification,
)
from transformers import (
    Trainer,
    TrainingArguments,
    EarlyStoppingCallback,
)
from os import makedirs
from os.path import join
import numpy as np
import json
import argparse


class OutcomesNER:
    """RCTClassifier"""

    def __init__(self, config_path: str):
        with open(config_path, "r") as file:
            config = json.load(file)
        self.config = config
        self.exp_name = (
            config_path.split("/")[-1].split(".")[0]
            + "_"
            + self.config["dataset_path"].split("/")[-1]
        )
        self.metric = load_metric("seqeval")

    def compute_metrics(self, p):
        predictions, labels = p
        predictions = np.argmax(predictions, axis=2)
        # Remove ignored index (special tokens)
        y_pred = [
            [self.label_list[p] for (p, l) in zip(prediction, label) if l != -100]
            for prediction, label in zip(predictions, labels)
        ]
        y_true = [
            [self.label_list[l] for (p, l) in zip(prediction, label) if l != -100]
            for prediction, label in zip(predictions, labels)
        ]
        return self.metric.compute(predictions=y_true, references=y_pred)

    def tokenize_and_align_labels(self, examples):
        tokenized_inputs = self.tokenizer(
            examples["tokens"],
            is_split_into_words=True,
            max_length=self.config["max_seq_length"],
            padding=True,
            truncation=True,
        )
        labels = []
        for i, label in enumerate(examples["labels"]):
            # Map tokens to their respective word.
            word_ids = tokenized_inputs.word_ids(batch_index=i)
            previous_word_idx = None
            label_ids = []
            for word_idx in word_ids:  # Set the special tokens to -100.
                if word_idx is None:
                    label_ids.append(-100)
                # Only label the first token of a given word.
                elif word_idx != previous_word_idx:
                    label_ids.append(label[word_idx])
                else:
                    label_ids.append(-100)
                previous_word_idx = word_idx
            labels.append(label_ids)
        tokenized_inputs["labels"] = labels
        return tokenized_inputs

    def prepare_data(self):
        disable_progress_bar()
        datasets = load_from_disk(self.config["dataset_path"])
        self.label_list = datasets["train"].features["labels"].feature.names
        self.label2id = {l: i for i, l in enumerate(self.label_list)}
        self.id2label = {i: l for i, l in enumerate(self.label_list)}
        self.tokenizer = AutoTokenizer.from_pretrained(self.config["model_name"])
        self.tokenized_datasets = datasets.map(
            self.tokenize_and_align_labels, batched=True
        )

    def model_init(self):
        return AutoModelForTokenClassification.from_pretrained(
            self.config["model_name"],
            num_labels=len(self.label_list),
            id2label=self.id2label,
            label2id=self.label2id,
        )

    def train(self):

        training_args = TrainingArguments(
            output_dir=join(self.config["train_out_dir"], self.exp_name),
            evaluation_strategy="steps",
            eval_steps=len(self.tokenized_datasets["train"])
            // self.config["batch_size"]
            + 1,
            save_steps=len(self.tokenized_datasets["train"])
            // self.config["batch_size"]
            + 1,
            load_best_model_at_end=True,
            save_total_limit=1,
            push_to_hub=False,
            disable_tqdm=True,
            warmup_ratio=self.config["warmup_ratio"],
            per_device_train_batch_size=self.config["batch_size"],
            per_device_eval_batch_size=self.config["batch_size"],
            learning_rate=self.config["learning_rate"],
            num_train_epochs=self.config["num_train_epochs"],
            weight_decay=0.01,
            seed=self.config["seed"],
        )

        data_collator = DataCollatorForTokenClassification(tokenizer=self.tokenizer)

        self.trainer = Trainer(
            model_init=self.model_init,
            args=training_args,
            train_dataset=self.tokenized_datasets["train"],
            eval_dataset=self.tokenized_datasets["validation"],
            tokenizer=self.tokenizer,
            compute_metrics=self.compute_metrics,
            data_collator=data_collator,
            callbacks=[
                EarlyStoppingCallback(early_stopping_patience=self.config["patience"]),
            ],
        )
        self.trainer.train()

    def save_test_results(self):
        results = self.trainer.predict(self.tokenized_datasets["test"])
        save_dir = join(self.config["test_out_dir"], self.exp_name)
        # Create output dir
        try:
            makedirs(save_dir, exist_ok=True)
        except OSError:
            print(f"Directory {save_dir} can not be created")
        # Save results in different files
        with open(f"{save_dir}/metrics.json", "w") as outfile:
            json.dump(results.metrics, outfile, indent=4)
        np.save(f"{save_dir}/predictions.npy", results.predictions)
        with open(f"{save_dir}/config.json", "w") as outfile:
            json.dump(self.config, outfile, indent=4)
        return results


if __name__ == "__main__":
    # Argument Parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", required=True, type=str)
    args = parser.parse_args()

    # Initialization and data loading
    out_ner = OutcomesNER(args.config)
    out_ner.prepare_data()
    # Training
    out_ner.train()
    # Evaluation on test set
    out_ner.save_test_results()
