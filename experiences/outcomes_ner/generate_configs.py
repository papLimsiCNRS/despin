import json
from os import makedirs
from os.path import join

outcome = "all-outcomes-primary-test"
template = json.load(open("./configs/template.json", "r"))
models_list = [
    ("dmis-lab/biobert-base-cased-v1.2", "biobert-1-2"),
    ("allenai/scibert_scivocab_uncased", "scibert-un"),
    ("microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract", "pubmedbert-abs"),
    ("michiyasunaga/BioLinkBERT-base", "biolinkbert"),
]

config = template
out_dir = join("configs", outcome)
makedirs(out_dir, exist_ok=True)
config["dataset_path"] = join("data", outcome)
for hf_model, alias in models_list:
    config["model_name"] = hf_model
    out_file = alias + ".json"
    with open(join(out_dir, out_file), "w") as f:
        json.dump(config, f, indent=4)
    