from datasets import Dataset, Value, ClassLabel, Features, Sequence, DatasetDict
from os.path import join
from os import listdir
from typing import List, Tuple, Dict
import json

def load_dataset(filelist: List[str], directory:str, label2id:Dict[str,int]) -> Tuple[List[List[str]],List[List[int]]]:
    sentences = []
    labels = []
    for filename in filelist:
        with open(join(directory, filename), 'r', encoding='utf-8') as input_file:
            sentence = []
            sentence_labels = []
            for l in input_file.readlines():
                l = l.strip()
                if l == '':
                    if len(sentence) > 0 and sentence not in sentences:
                        sentences.append(sentence)
                        labels.append(sentence_labels)
                    sentence = []
                    sentence_labels = []
                else:
                    token, tag = l.split()
                    sentence.append(token)
                    sentence_labels.append(label2id[tag])
    return sentences, labels

def get_full_dataset(sentences:List[List[str]], labels:List[List[int]], label2id:Dict[str,int]) -> Dataset:
    """Create Dataset obj from BIO extracted data"""
    examples_dict = {
            'id': [i for i in range(len(sentences))],
            'labels': labels,
            'tokens': sentences,
    }
    id2label = [0 for _ in label2id.keys()]
    for k,v in label2id.items():
        id2label[v] = k
    features = Features({
        "id" : Value('uint16'),
        "labels" : Sequence(ClassLabel(len(label2id.keys()),names=id2label)),
        "tokens" : Sequence(Value('string')),
    })
    return Dataset.from_dict(examples_dict, features=features)

def split_dataset(dataset:Dataset, seed=42) -> DatasetDict:
    train_test = dataset.train_test_split(0.2, seed=seed)
    test_val = train_test["test"].train_test_split(0.5, seed=seed)
    dataset_dict = DatasetDict({
        "train" : train_test["train"],
        "validation" : test_val["train"],
        "test" : test_val["test"] 
    })
    return dataset_dict


def create_hf_dataset(raw_dataset_path:str) -> DatasetDict:
    txt_filelist = [f for f in listdir(raw_dataset_path) if f.endswith(".txt")]
    json_file = [f for f in listdir(raw_dataset_path) if f.endswith(".json")][0]
    with open(join(raw_dataset_path,json_file), 'r') as f:
        label2id = json.load(f)
    sentences, labels = load_dataset(txt_filelist, raw_dataset_path, label2id)
    full_dataset = get_full_dataset(sentences, labels, label2id)
    dataset_dict = split_dataset(full_dataset, seed=42)
    return dataset_dict


if __name__ == "__main__":
    # folder containing primary/reported/primary_secondary outcomes data subfolders
    outcomes_data_folder = "./data/"

    for outcome in listdir(outcomes_data_folder): 
        raw_dataset_path = join(outcomes_data_folder, outcome)
        dataset_dict = create_hf_dataset(raw_dataset_path)
        dataset_dict.save_to_disk(raw_dataset_path)
    