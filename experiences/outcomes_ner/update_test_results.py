import os
import json
import pandas as pd


def get_test_results(test_dir) -> pd.DataFrame:
    result_list = []
    for subdir in os.listdir(test_dir):
        res_dict = {}
        res_dict["model"], res_dict["dataset"] = subdir.split("_")
        with open(os.path.join(test_dir, subdir, "metrics.json"), "r") as f:
            model_result = json.load(f)
        for k, v in model_result.items():
            if "test_overall" in k:
                res_dict[k.split("_")[-1]] = v
        with open(os.path.join(test_dir, subdir, "config.json"), "r") as f:
            config = json.load(f)
        for k, v in config.items():
            if k not in ["train_out_dir", "test_out_dir", "dataset_path", "model_name"]:
                res_dict[k] = v
        result_list.append(res_dict)
    return pd.DataFrame(result_list)


def combine_dataframes(new_df, last_df_csv_path):
    last_df = pd.read_csv(last_df_csv_path, index_col=0)
    concat_df = pd.concat([last_df, new_df], axis=0)
    subset = ["model","dataset","max_seq_length","batch_size","learning_rate","warmup_ratio","num_train_epochs","seed","patience"]
    concat_df.drop_duplicates(inplace=True, keep="first", ignore_index=True, subset=subset)
    return concat_df


def update_results(test_dir, results_file):
    new_df = get_test_results(test_dir)
    if os.path.isfile(results_file):
        result_df = combine_dataframes(new_df, results_file)
    else:
        result_df = new_df
    result_df.sort_values(by="f1", ascending=False, ignore_index=True, inplace=True)
    result_df.to_csv(results_file)


if __name__ == "__main__":
    test_dir = "out/test"
    results_file = "global_results/primary.csv"
    update_results(test_dir, results_file)
