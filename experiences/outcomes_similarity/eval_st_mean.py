from os import listdir
from os.path import join
from typing import List
import pandas as pd
import json

class STMeanEvaluator:
    """ Mean evaluator for multiple sentence transformers models output"""
    def __init__(self, metric:str="cossim"):
        self.metric = metric
        
    def get_best(self, dir_path:str):
        df = pd.read_csv(join(dir_path,"binary_classification_evaluation_results.csv"))
        # getting line where f1 is max
        best_result_serie = df.iloc[df[f"{self.metric}_f1"].idxmax()] 
        # converting to dictionary
        best_result_dict = {k:v for k,v in best_result_serie.to_dict().items() if (self.metric or "epoch") in k}
        return best_result_dict
    
    def get_mean_results(self, eval_dir_list:List[str], output_file_path="./mean_results.json"):
        dict_list = [self.get_best(eval_dir) for eval_dir in eval_dir_list]
        mean = dict(pd.DataFrame(dict_list).mean())
        with open(output_file_path,"w") as f:
            json.dump(mean,f,indent=4)

if __name__ == "__main__" :
    TEST_RESULTS_DIR = "/mnt/beegfs/home/laiking/projets/replication/outcomes_similarity/test_results"
    INPUT_LIST = [join(TEST_RESULTS_DIR,sub) for sub in listdir(TEST_RESULTS_DIR)]
    
    evaluator = STMeanEvaluator()
    evaluator.get_mean_results(INPUT_LIST, "./unsup_results/all-mpnet-base-v2.json")