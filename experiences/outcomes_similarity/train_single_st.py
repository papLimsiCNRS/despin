from sentence_transformers import SentenceTransformer, InputExample, losses, evaluation
from datasets import load_from_disk, concatenate_datasets
from torch.utils.data import DataLoader
from argparse import ArgumentParser
import json

class STTrainer:

    def __init__(self, config_path:str) -> None :
        with open(config_path,'r') as f :
            config = json.load(f)
        self.config = config
        self.output_name = self.config["model_name"] + "_" + str(self.config["split"])
        
    def load_dataset(self, dataset_path:str="", train_all_data=False):
        # Loading Dataset
        if not dataset_path :
            dataset_path = self.config["dataset_path"] + str(self.config["split"])
        dataset = load_from_disk(dataset_path)
        # Defining training set
        if train_all_data:
            training_set = concatenate_datasets([dataset["train"],dataset["dev"]])
        else :
            training_set = dataset["train"]
        # training set
        train_examples = [InputExample(
                    guid = str(example['sent_id']),
                    texts = [example["text"], example["text_pair"]],
                    label = float(example["label"])
                ) for example in training_set]
        self.train_dataloader = DataLoader(train_examples, shuffle=True, batch_size = self.config["batch_size"])
        # dev set
        self.evaluator = evaluation.BinaryClassificationEvaluator(
            sentences1=dataset["dev"]["text"],
            sentences2=dataset["dev"]["text_pair"],
            labels=dataset["dev"]["label"],
            batch_size=self.config["batch_size"],
        )
    
    def load_model(self, model_name:str=""):
        if not model_name:
            model_name = self.config["model_name"]
        self.model = SentenceTransformer(model_name)

    def train(self, loss='cosine'):
        self.load_model()
        if loss=="cosine":
            train_loss = losses.CosineSimilarityLoss(self.model)
        self.model.fit(
            train_objectives=[(self.train_dataloader,train_loss)],
            evaluator=self.evaluator,
            epochs = self.config["epochs"],
            evaluation_steps=self.config["evaluation_steps"],
            warmup_steps = self.config["warmup_steps"],
            output_path = "./sentence_transformers_train/" + self.output_name,
        )
    

    
if __name__ == "__main__" :
    parser = ArgumentParser()
    parser.add_argument("--config", required=True, type=str)
    args = parser.parse_args()

    stt = STTrainer(args.config)
    stt.load_dataset(train_all_data=True)
    stt.train()
    