from datasets import Dataset, Value, ClassLabel, Features, Sequence, DatasetDict
import os
import pandas as pd


def get_hf_dataset(tsv_file_path:str) -> Dataset:
    df = pd.read_csv(tsv_file_path, sep="\t")
    df.rename(columns = {'out1':'text', 'out2':'text_pair'}, inplace = True)
    df.drop(['id1','id2'], axis=1, inplace=True)
    features = Features({
        "sent_id" : Value('uint16'),
        "label" : ClassLabel(2,names=["different","similar"]),
        "text" : Value('string'),
        "text_pair" : Value('string'),
    })
    return Dataset.from_pandas(df, features=features)


def combine_datasets(splits_path:str, k_fold=10):
    k_fold_dict = {}
    for k in range(k_fold):
        splits_dict = {}
        for split in ['train','dev'] :
            tsv_file_path = os.path.join(splits_path,str(k),split + '.tsv')
            dataset = get_hf_dataset(tsv_file_path)
            splits_dict[split] = dataset
        k_fold_dict[str(k)] = DatasetDict(splits_dict)
    return DatasetDict(k_fold_dict)

if __name__ == "__main__":
    ## YOUR PATH TO THE DESPIN OUTCOME SIMILARITY DATASET SPLITS 
    # must contain 0,1,2,3,4,5,6,7,8,9 subfolders, themselve containing train.tsv & dev.tsv each)
    splits_path = '/people/laiking/data/despin/outcome_similarity/splits/'
    ## Output dataset path (huggingface format)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    output_path = os.path.join(dir_path,'./data/')

    ## Dataset creation and saving
    splits_datasetdict = combine_datasets(splits_path)
    splits_datasetdict.save_to_disk(output_path)
