import os
import json
import pandas as pd
from typing import List


def mean(l: List[float]) -> float:
    return sum(l)/len(l)

class ModelResults :

    def __init__(self, model_results_path: str) -> None:
        self.model_name = model_results_path.split(os.sep)[-1]
        self.metrics = ["test_f1","test_accuracy","test_precision","test_recall"]
        self.results = self.get_metrics_mean(model_results_path)
    
    def get_metrics_mean(self, model_path:str, k_fold=10) -> float:
        metrics_dict = {metric:[] for metric in self.metrics}
        for split in range(k_fold):
            with open(os.path.join(model_path,str(split),"metrics.json")) as f: 
                dic = json.load(f)
                for metric in self.metrics:
                    metrics_dict[metric].append(dic[metric])
        return {metric:mean(metrics_dict[metric]) for metric in self.metrics}
    
    def as_dict(self) -> dict:
        return {
            "model": self.model_name,
            "f1": self.results["test_f1"],
            "accuracy": self.results["test_accuracy"],
            "precision": self.results["test_precision"],
            "recall": self.results["test_recall"]
        }


def main():
    current_file_dir = os.path.dirname(os.path.realpath(__file__))
    results_dir = os.path.join(current_file_dir,"test_results")
    models_results_paths = [os.path.join(results_dir,m) for m in os.listdir(results_dir)]
    
    models_results = []
    for model_results_path in models_results_paths:
        mr = ModelResults(model_results_path)
        models_results.append(mr.as_dict())
    
    df = pd.DataFrame(models_results)
    df.to_markdown(os.path.join(current_file_dir,"models_results.md"))
    df.to_csv(os.path.join(current_file_dir,"models_results.tsv"), sep="\t")

if __name__ == "__main__":
    main()