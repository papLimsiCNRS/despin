from json import dump
from typing import Dict, Any
from os.path import exists, join
from os import makedirs

CONFIG_OUTPUT_DIR = "/people/laiking/projets/despin/experiences/outcomes_similarity/st_configs"
BASE_CONFIG = {
    "model_name" : "all-mpnet-base-v2",
    "dataset_path" : "/people/laiking/data/despin/outcome_similarity/splits",
    "batch_size" : 32,
    "epochs" : 5,
    "warmup_steps" : 100,
    "evaluation_steps" : 100
}

variable_params = {
    "split" : [i for i in range(10)]
}

def write_json_file(dict: Dict[str,Any], output_file:str) -> None:
    with open(output_file, 'w') as f:
        dump(dict,f, indent=4)

if not exists(CONFIG_OUTPUT_DIR):
    makedirs(CONFIG_OUTPUT_DIR)

for param_name, param_values in variable_params.items():
    for param_value in param_values:
        current_cfg = BASE_CONFIG
        current_cfg[param_name] = param_value
        output_filename = join(CONFIG_OUTPUT_DIR , BASE_CONFIG["model_name"] + "_" + param_name + str(param_value) + ".json")
        write_json_file(current_cfg, output_filename)