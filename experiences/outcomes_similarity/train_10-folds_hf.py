from datasets import load_from_disk, load_metric
from transformers import Trainer, TrainingArguments, AutoModelForSequenceClassification, AutoTokenizer, DataCollatorWithPadding
from os.path import join, dirname, realpath
from os import makedirs
import numpy as np
import json


metrics = [load_metric('f1'),load_metric('precision'),load_metric('recall'),load_metric('accuracy')]

def compute_metrics(p):
    predictions, labels = p
    predictions = np.argmax(predictions, axis=-1)
    return {metric.name : metric.compute(predictions=predictions, references=labels)[metric.name] for metric in metrics}


def save_results(results, output_dir):
    try:
        makedirs(output_dir, exist_ok = True)
    except OSError:
        print(f"Directory {output_dir} can not be created")
    # Save test results
    with open(output_dir + "metrics.json", "w") as outfile:
        json.dump(results.metrics, outfile)
    np.save(output_dir + "predictions.npy", results.predictions)
    np.save(output_dir + "label_ids.npy", results.label_ids)

if __name__ == "__main__":
    model_name = 'monologg/biobert_v1.0_pubmed_pmc'

    # Loading Tokenizer and DataCollator
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

    # Getting datapath
    dir_path = dirname(realpath(__file__))

    for i in range(10): # k-fold cross validation
        ## Creating model
        model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=2)  

        model_split_name = join(model_name.split('/')[-1], str(i),'')

        ## Directories
        checkpoints_out_dir = join(dir_path,"training_checkpoints",model_split_name)
        test_results_dir = join(dir_path,"test_results",model_split_name)
        split_dir = join(dir_path,"data",str(i))
        
        ## Load DatasetDict from current split and tokenize it
        dataset = load_from_disk(split_dir)
        tokenize_func = lambda example:tokenizer(text=example['text'], text_pair=example['text_pair'])
        tokenized_dataset = dataset.map(tokenize_func, batched=True)

        # Hyperparameters
        training_args = TrainingArguments(
            output_dir=checkpoints_out_dir ,
            evaluation_strategy="steps",
            per_device_train_batch_size=32,
            per_device_eval_batch_size=8,
            learning_rate=5e-5,
            load_best_model_at_end=True,
            num_train_epochs=3,
            warmup_ratio=0.1,
            logging_steps=100,
            save_steps=100,
            save_strategy="steps",
            save_total_limit=1,
            metric_for_best_model='f1',
            seed=42,
        )

        # creating Trainer
        trainer = Trainer(
            model=model,
            args=training_args,
            train_dataset=tokenized_dataset["train"],
            eval_dataset=tokenized_dataset["dev"],
            tokenizer=tokenizer,
            compute_metrics=compute_metrics,
            data_collator=data_collator,
        )

        ## starting training
        trainer.train()

        ## save best model and remove checkpoints directories (because of memory limit)
        trainer.save_model()
        
        ## predict on test set
        test_results = trainer.predict(tokenized_dataset["dev"])

        save_results(test_results, test_results_dir)
