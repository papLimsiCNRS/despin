# author: pap@limsi.fr Mon Apr 25 15:12:18 CEST 2022
# providing parallel reading a "corpus" made of several opened files, each file containing ,
# reading one "document" at each call to corpus_loader.next_document( fname ) with fname a str path/file name,
# when all the files have been read, the call to corpus_loader.next_document( fname ) returns an endcorpus object
# when a file end is reached, the call to corpus_loader.next_document( fname ) returns an endfile object.
# Depending on you corpus format, a file may hold one single document or several documents.
# Note that the constructor of the class corpus_loader expects an optional funarg
# for reading one document from an input file stream, it is set by default to lambda x : x.read()
# which reads the whole file in one shot.

import os
import sys
import io
import glob
import json
import re

from pathlib import Path
from configparser import ConfigParser
from pprint import pprint

from text_span_v2 import document

conf_parser = ConfigParser( allow_no_value = True )
conf_file = Path( __file__ ).parent / "config.ini"
conf_parser.read( conf_file )

FILE_PATH_SEP = '/'  # for Unix, MacOS and Android

#DEBUG = True
DEBUG = False

DEMO = True
#DEMO = False


def eof_p( f ):
  lastpos = f.tell()    
  f.seek(0, os.SEEK_END)
  end = f.tell()    
  f.seek( lastpos, os.SEEK_SET)
  return lastpos == end

class begfile( object ):
    def __init__( self, fname = None ):
        assert( (type( fname ) is str) and (fname != '') )
        self.fname = fname

    def __str__( self ):
        return 'BEGFILE_fname=={0}'.format( self.fname )

class nextdoc( object ):
    def __init__( self, fname = '', doc = None):
        assert( (type( fname ) is str) and (len( fname ) > 0) )
        assert( (type( doc ) is document) and (doc.len() > 0) )
        self.doc = doc
        self.fname = fname

    def id( self ):
        return self.fname

    def __str__( self ):
        return 'nextdoc_fname=={0}_id=={1}'.format( self.fname, self.id() ) 

class endfile( object ):
    def __init__( self, fname ):
        assert( (type( fname ) is str) and (fname != '') )
        self.fname = fname

    def __str__( self ):
        return 'ENDFILE_fname=={0}'.format( self.fname )

class endcorpus( object ):
    def __init__( self ):
        pass

    def __str__( self ):
        return 'ENDCORPUS'

class corpus_loader( object ):
    def __init__( self, data_dir = '/tmp/', file_nm_lst = [], insufx = '.txt', next_doc_reader_fun = lambda x : x.read() ):
        assert( (type( file_nm_lst ) is list) and (file_nm_lst != []) )
        assert( False not in [ (type( x ) is str) and ( x != '' ) for x in file_nm_lst ] )
        self.data_dir = data_dir
        if data_dir[ -1 ] != FILE_PATH_SEP:
            self.data_dir += '/'
        assert( False not in [ os.path.isfile( self.data_dir + x + insufx ) for x in file_nm_lst ] )
        self.first_unopened_fidx = 0
        self.file_nm_lst = file_nm_lst
        self.insufx = insufx
        if DEBUG:
            print( 'DEBUG corpus loader file_nm_lst in init() is == {0}'.format( file_nm_lst ))
        self.open_files = [ None ] * len( file_nm_lst )
        self.already_processed_docids = [ [] ] * len( file_nm_lst )
        # NOTE theses counts can only be used to test the end of the buffer when reading, to to identify by position elements in the buffer!
        # The ONLY way to identify an element in the buffer is by its id, because we want to be able to load a list of already processed
        # ids which cannot put hypotheses upon the order in which this already processed items have been handled in the past!
        self.doc_cnt_lst = [ 0 ] * len( file_nm_lst ) 
        #-----
        # MAPA specific stuff
        self.subcorpora = [ None ] * len( file_nm_lst )
        self.next_doc_reader_fun = next_doc_reader_fun  # the function that takes a file stream opened in read text mode and returns the next document of the file or endcorpus.
        #-----

    def __str__( self ):
        msg = ' corpus_loader object -----------\n'
        msg += '\tdata_dir== {0}\n'.format( self.data_dir )
        msg += '\tfirst_unopened_fidx== {0}\n'.format( self.first_unopened_fidx )
        msg += '\tfile_nm_lst== {0}\n'.format( self.file_nm_lst )
        msg += '\tself.open_files== {0}\n'.format( self.open_files )
        msg += '\tself.already_processed_docids== {0}\n'.format( self.already_processed_docids )
        msg += '\tself.doc_cnt_lst== {0}\n'.format( self.doc_cnt_lst )
        msg += '\tself.subcorpora== {0} ...<first 100 chars only>...\n'.format( self.subcorpora[0 : min( 100,
                                                                                                         len( self.subcorpora ))])
        return msg
        
    def open_file( self ):
        assert( len( self.file_nm_lst ) > 0)
        if self.first_unopened_fidx < len( self.file_nm_lst ):
            curr_fidx = self.first_unopened_fidx
            self.curr_file_path = self.data_dir + self.file_nm_lst[ curr_fidx ] + self.insufx
            if DEBUG:
                print( 'DEBUG self.curr_file_path = {0}'.format( self.curr_file_path ))
                sys.stdout.flush()
            try:
                self.curr_in_desc = self.open_files[ curr_fidx ] = open( self.curr_file_path, 'rt' )
                self.subcorpora[ curr_fidx ] = self.curr_in_desc 
                if DEBUG:
                    print( '_________________________' )
                    #print( 'there are {0} documents in {1}'.format( len( self.subcorpora[ curr_fidx ] ), self.curr_file_path ) )
                    print( '_________________________' )
                    sys.stdout.flush()
            except IOError as e:
                print( 'Exception {0} was raised while opening the file {1}'.format( e, self.curr_file_path ) )
                assert( False )
            self.first_unopened_fidx += 1
            return self.file_nm_lst[ curr_fidx ]
        else:
            return None
            
    def file_opened( self, fidx = None):
        assert( (fidx != None) and type(  fidx is int) and (0 <=  fidx) and (fidx <= len( self.subcorpora ) ) )
        return self.open_files[ curr_fidx ] != None

    def close_file( self, fidx = None ):
        assert( (fidx != None) and type(  fidx is int) and (0 <=  fidx) and (fidx <= len( self.subcorpora ) ) )
        if self.open_files[ fidx ] != None :
            if not self.open_files[ fidx ].closed:
                self.open_files[ fidx ].close()
                self.open_files[ fidx ] = None

    def reset( self ):
        for i in range( 0, len( self.file_nm_lst )):
               if self.open_files[ i ] is not None:
                   self.open_files[ i ].close()
                   self.open_files[ i ] = None
        self.first_unopened_fidx = -1
        self.already_processed_docids = [ [] ] * len( self.file_nm_lst )
        self.doc_cnt_lst = [ 0 ] * len( self.file_nm_lst )
        self.subcorpora = [ None ] * len( self.file_nm_lst )
        return self.open_file() # return the name of the first file that has just been opened

    def has_unopened_files( self ):
        return self.first_unopened_fidx < len( self.file_nm_lst )
        
    def next_file( self ):
        # NOTE: I cannot use an iterator() here since this code will be used to communicate
        # between the mapper and the reducer for opening and closing output files
        # at each change of input file and otherwise send the current document to the reducer;
        # since iterator() use the StopIteration exception an iterator would work across
        # the two virtual processors memory spaces.
        if self.has_unopened_files():
            return begfile( self.open_file() )
        else:
            # warn the vprocessor reducer that we reached the end of the corpus
            return endcorpus()
        
    def next_document( self, fnm = None ):
        if DEBUG:
            print( '~~~~~~~~~~~~~~~~~DEBUG in corpus_loader class, next_document( fnm= {0}'.format( fnm ))
            print( 'DEBUG next_document() fnm== {0}'.format( fnm ))
            print( 'DEBUG self.first_unopened_fidx== {0}'.format( self.first_unopened_fidx ))
            sys.stdout.flush()
        if fnm is None :
            if self.first_unopened_fidx < len( self.file_nm_lst ):
                return begfile( fname = self.open_file() ) # by default the first yet unopened file will be opened
            else:
                return endcorpus()  
        else:
            assert( fnm in self.file_nm_lst )
            query_fidx = self.file_nm_lst.index( fnm )
            assert( self.open_files[ query_fidx ] is not None )
            already_processed_docid = True
            while already_processed_docid:
                if not eof_p( self.open_files[ query_fidx ] ):
                    try:
                        new_doc_content = self.next_doc_reader_fun( self.open_files[ query_fidx ] )
                        # the document id is composed of '{0}_{1}_{2}' with:
                        #   0 is the file name ;
                        #   1 is the document integer index in the file (starting at 0) ;
                        #   2 is the document file position as returned by tell().
                        new_doc = document( id = '{0}_{1}_{2}'.format( self.file_nm_lst[ query_fidx ], self.doc_cnt_lst[ query_fidx ], self.open_files[ query_fidx ].tell() ),
                                            content = new_doc_content )
                        already_processed_docid = (new_doc.id in self.already_processed_docids[ query_fidx ])
                    except IOError as e:
                        print( 'IOError reading document index {0} in file {1} while at file position {2}'.format( self.doc_cnt_lst[ query_fidx ] + 1,
                                                                                                               fnm,
                                                                                                               self.open_files[ query_fidx ].tell() ))
                        assert( 0 )
                    self.already_processed_docids[ query_fidx ] = new_doc.id
                    self.doc_cnt_lst[ query_fidx ] += 1
                    return nextdoc( fname = fnm, doc = new_doc )
                else:
                    return self.next_file()

    def set_prev_runs_log_file_lst( self, in_fname = None, prev_runs_log_flst = None ):
        assert( (type( in_fname ) is str) and (in_fname != ''))
        assert( type( prev_runs_log_flst is list ) )
        assert( False not in [ ((type( x ) is str) and (len( x ) > 0)) for x in prev_runs_log_flst] )
        # when a file is opened, the reducer opens output file and check whether there exist
        # some previous run log file(s) for the given input_fname, each previous log file for a given
        # run will hold the list of ids contained in the input_fname file that were processed
        # during that previous run.
        for logf in prev_runs_log_flst:
            try:
                with  open( logf, 'rt' ) as logfdesc:
                    self.already_processed_docids[ self.file_nm_lst.index( in_fname ) ] += [ x.strip() for x in logfdesc.readlines() ]
            except IOError as e:
                print( 'WARNING ERROR reading previous log file {0}'.format( logf ))

#----- end of class corpus_loader

## TODO loading the already processed elements should be the responsibility of the corpus loader here
## the mapper should have only to process yet unprocessed elements !!!!!!!!!!!

class result_exporter ( object ):
    def __init__( self, out_data_dir = '', output_labels = []  ):
        # output_funs is a dictionary holding the label of each kind of output associated to a document as keys
        # and the corresponding output functions, each one having as parameters the result associated to the document
        # and an output stream (which will have been opened by the result_exporter class at initialization
        assert( (type( out_data_dir ) == str) and (out_data_dir != '')  and  os.path.isdir( out_data_dir ) )
        self.out_data_dir = out_data_dir
        self.outfiles = {}
        self.output_labels = output_labels
        for outyp in self.output_labels:
            assert( (type( outyp ) is str) and (outyp != '') )
        
    def open_outfiles( self, input_file_nm ):
        assert( (type( input_file_nm ) is str) and (input_file_nm != ''))
        # in the following log file we will store the ids of the documents processed so far.
        # in case of an interrupt/restart we will read the ids of the documents processed so far
        # and restart working from the first unprocessed document, storing the outputs
        # in files whose name is parametrized by the number of restarts, like with the name of the log file.
        prev_log_file_lst = glob.glob( re.sub('\.json$', '', self.out_data_dir + input_file_nm ) + '_out*.log' )
        prev_log_file_lst.sort()
        log_cnt = 0
        self.outfiles[ input_file_nm ] = {}  
        if len( prev_log_file_lst ) > 0:
            previous_out_log = prev_log_file_lst[ -1 ]
            log_cnt  = int( re.search( '_out([0-9]+)\.log', previous_out_log ).group( 1 )) + 1  # preparing the next run log file
            (b, e) = re.search( '_out([0-9]+)\.log', previous_out_log ).span( 1 )
            self.outfiles[ input_file_nm ][ 'idlog' ] = previous_out_log[ 0:b ] + '{0}'.format( log_cnt ) + previous_out_log[ e: ] # next run log file name
        else:
            self.outfiles[ input_file_nm ][ 'idlog' ]  = re.sub('\.json$', '', self.out_data_dir + input_file_nm  ) + '_out0.log' # first run log file name
        #if DEBUG:
        print( 'DEBUG +++++++++ opened out log file == {0}'.format( self.outfiles[ input_file_nm ][ 'idlog' ] ))
        sys.stdout.flush()
        self.outfiles[ input_file_nm ][ 'idlog_fdesc' ] = open( self.outfiles[ input_file_nm ][ 'idlog' ], 'wt' )
        #---
        for outfile_key in self.output_labels:
            if outfile_key in self.outfiles[ input_file_nm ].keys():
              if outfile_key + '_fdesc' in self.outfiles[ input_file_nm ]:
                if (self.outfiles[ input_file_nm ][ outfile_key + '_fdesc' ] is not None) and (not self.outfiles[ input_file_nm ][ outfile_key + '_fdesc' ].closed):
                  self.outfiles[ input_file_nm ][ outfile_key + '_fdesc' ].close()
            else:
              self.outfiles[ input_file_nm ][ outfile_key ] = {}
            self.outfiles[ input_file_nm ][ outfile_key ] = self.out_data_dir + input_file_nm + '_' + outfile_key + '_out{0}'.format( log_cnt )  + '.csv'
            self.outfiles[ input_file_nm ][ outfile_key + '_fdesc' ] = open( self.outfiles[ input_file_nm ][ outfile_key ], 'wt' )
        return prev_log_file_lst 

    def output_item( self, in_fname, docid, outdata_dic ):
        #if DEBUG:
        print( 'output_item() in_fname== {0} docid== {1}, outdata_dic== {2}'.format( in_fname, docid, outdata_dic ))
        assert( (type( in_fname ) is str) and (in_fname != '') )
        assert( type( outdata_dic ) is dict )
        assert( in_fname in list( self.outfiles.keys()) )
        assert( ('idlog' in self.outfiles[ in_fname ]) and (self.outfiles[ in_fname ][ 'idlog'] is not None))
        assert( (self.outfiles[ in_fname ][ 'idlog_fdesc' ] is not None) and (not self.outfiles[ in_fname ][ 'idlog_fdesc' ].closed))
        self.log_processed_doc( in_fname, docid )
        for outfile_key in self.output_labels:
          try:
            assert( outfile_key in self.outfiles[ in_fname ].keys() )
            assert( outfile_key + '_fdesc' in self.outfiles[ in_fname ].keys() )
            if (self.outfiles[ in_fname ][ outfile_key +  '_fdesc' ] is not None) and (not self.outfiles[ in_fname ][ outfile_key + '_fdesc' ].closed):
              self.outfiles[ in_fname ][ outfile_key +  '_fdesc' ].write( '{0}'.format( outdata_dic[ outfile_key ] ) )
              self.outfiles[ in_fname ][ outfile_key +  '_fdesc' ].flush()
          except:
            print( 'Exception raised when outputting results of type {0} in {1}'.format( outfile_key, self.outfiles[ in_fname ][ outfile_key ]))
            sys.stdout.flush()

    def log_processed_doc( self, in_fname, docid ):
        if DEBUG:
            print( 'DEBUG log_processed_doc nxtdoc== {0}'.format( docid ))
            sys.stdout.flush()
        try:
            self.outfiles[ in_fname ][ 'idlog_fdesc' ].write( '{0}\n'.format( docid ))
            self.outfiles[ in_fname ][ 'idlog_fdesc' ].flush()
        except IOError as e:
            print( 'IOError exception raised in mapper.log_processed_doc() with in_fname== {0} docid== {1}'.format( in_fname, docid  ))
            sys.stdout.flush()
            assert( 0 )

    def close_outfile( self, outfnm ):
        if 'idlog' in self.outfiles[ outfnm ]:
            if (self.outfiles[ outfnm ][ 'idlog' ] is not None):
                del self.outfiles[ outfnm ][ 'idlog' ]
                if (self.outfiles[ outfnm ][ 'idlog_fdesc' ] is not None) and (not self.outfiles[ outfnm ][ 'idlog_fdesc' ].closed):
                    self.outfiles[ outfnm ][ 'idlog_fdesc' ].close()
                    del self.outfiles[ outfnm ][ 'idlog_fdesc' ]
        for outfile_key in self.output_labels:
            if outfile_key in self.outfiles[ outfnm ]:
                if (self.outfiles[ outfnm ][ outfile_key ] is not None):
                    del self.outfiles[ outfnm ][ outfile_key ]
            if (self.outfiles[ outfnm ][ outfile_key + '_fdesc' ] is not None) and (not self.outfiles[ outfnm ][ outfile_key + '_fdesc' ].closed):
                self.outfiles[ outfnm ][ outfile_key + '_fdesc' ].close()
                del self.outfiles[ outfnm ][ outfile_key + '_fdesc' ]

    def close_all_outfiles( self ):
        for outfnm in self.outfiles:
            self.close_outfile( outfnm )

#-------------------
if DEMO:
    def demo():
        the_file_lst = conf_parser.get( 'configuration', 'input_file_lst' )
        indir = conf_parser.get( 'configuration', 'input_dir' )
        insufx = conf_parser.get( 'configuration', 'insufx' )
        outdir = conf_parser.get( 'configuration', 'outdir' )

        #----- corpus input ------
        # the next_doc_reader_fun which reads (parse) the next document from an opened input file stream
        def get_next_document( fdesc ):
            assert( type( fdesc ) is io.TextIOWrapper )
            # return fdesc.read()  # for making each file a single document
            return fdesc.readline()  # for making each line a single document in this demo

        data_loader = corpus_loader(  data_dir = indir,
                                      file_nm_lst  =  [ x  for x in open( the_file_lst, 'rt' ).read().strip().split()],
                                      insufx = insufx,
                                      next_doc_reader_fun = get_next_document )

        # ------ results output ----------------------
        # three dummy outputs for demo purpose (in addition to the document id log output which is always present).
        lineOutLabel = 'lineOut'
        textOutLabel = 'textOut'
        reversedOutLabel = 'reversedOutLabel'

        def line_process( doc ):
          assert( type( doc ) is document )
          return doc.ctnt.strip().split()

        def text_process( doc ):
          assert( type( doc ) is document )
          return doc.ctnt

        def reverse_process( doc ):
          assert( type( doc ) is document )
          return list( map( lambda x : x[::-1], (doc.ctnt.strip().split())[::-1] ))

        process_funs = { lineOutLabel : line_process, textOutLabel : text_process, reversedOutLabel : reverse_process }
        
        data_exporter = result_exporter( out_data_dir = outdir, output_labels = list( process_funs.keys()) )
        #-------- corpus processing -----------

        def document_processor( doc, process_funs ):
          assert( type( doc ) is document )
          res = {}
          for outkey in process_funs.keys():
            res[ outkey ] = process_funs[ outkey ]( doc )
          return res
          
        already_processed_docids = {}
        curr_fnm = None
        e = None
        while type( e ) != endcorpus:
            e = data_loader.next_document( curr_fnm )
            print( 'type(e)== {0} e== {1} curr_fnm== {2}'.format( type( e ), e, curr_fnm ))
            if type( e ) == begfile:   # Note that begfile event is important for managing output files in function of current input files
                if curr_fnm is not None:
                  data_exporter.close_outfile( curr_fnm )
                curr_fnm = e.fname
                already_processed_docids[ curr_fnm ] = [] 
                prev_log_file_lst = data_exporter.open_outfiles( curr_fnm )
                for f in prev_log_file_lst:
                      already_processed_docids[ curr_fnm ] +=  open( f, 'rt' ).read().strip().split()
                print( 'already processed docids for file {0} is {1}'.format( curr_fnm, already_processed_docids[ curr_fnm ] ))
            elif type( e ) is nextdoc:
                curr_fnm = e.fname
                assert( curr_fnm is not None )
                if e.doc.id in already_processed_docids[ curr_fnm ]:
                  print( 'in file {0} document with id {1} has already been processed, skiping it.'.format( curr_fnm, e.doc.id ))
                else:
                  print( 'processing\n\te.id== {0}\n\tdoc.id== {1}\n\tdoc.ctnt== {2}\n\t'.format( e.id(), e.doc.id, e.doc.ctnt ) )
                  res = document_processor( e.doc, process_funs )
                  print( 'in file {0} processed document {1}'.format( e.fname, e.doc.id ) )
                  print( 'res== {0}'.format( res ))
                  data_exporter.output_item( curr_fnm, e.doc.id, res )
            elif type( e ) is endfile:
                curr_fnm = None
            else:
                print( 'e=={0}'.format( e ))
                assert( type( e ) is endcorpus )
            print( '______ type( e )== {0} curr_fnm== {1}'.format( type( e ), curr_fnm ))
    #-------
    demo()
        


